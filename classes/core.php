<?php

namespace BlueMammoth\Carousel;

/**
 * Class Core
 *
 * @package BlueMammoth\Carousel
 */
class Core {

    /**
     * Core constructor.
     */
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_assets'));
        add_shortcode('hor-carousel', array($this, 'display'));
    }

    /**
     * Displays the carousel
     *
     * @param array|string $atts
     * @return string
     */
    public function display($atts) {
        $atts = shortcode_atts(array(
            'duration' => '2000',
            'keys'  => '',
        ), $atts);
        return '<div class="hor_carousel_wrapper" data-duration="' . $atts['duration'] . '" style="position: relative;margin: 0;padding: 0;display: inline-block;" data-quotes="' . htmlentities($atts['keys']) . '"><span class="quote"></span></div>';
    }

    /**
     * Enqueue assets
     */
    public function enqueue_assets() {
        wp_enqueue_script('carousel-js', plugins_url('js/carousel.js', HOR_CAROUSEL_FILE), array('jquery'));
    }
}