<?php

/**
 * Plugin Name: Het Online Recept text carousel
 * Plugin URI:  https://bluemammoth.nl/
 * Description: Text carousel
 * Author:      Blue Mammoth: Saif Rashed
 * Author URI:  https://bluemammoth.nl/
 * Version:     0.1.0
 */
define('HOR_CAROUSEL_FILE', __FILE__);

require_once __DIR__ . '/classes/core.php';

new BlueMammoth\Carousel\Core();

