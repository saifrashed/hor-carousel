/**
 * @author: Blue Mammoth: Saif Rashed
 * @description: Javascript text carousel.
 */
(function ($) {
    $(function () {

        $('.hor_carousel_wrapper').each(function () {

            var display = $(this).find('.quote');
            var quotes = String($(this).attr('data-quotes')).split(',');

            display.css({
                opacity: '1',
            }).text(quotes[0]);

            var counter = 0;
            var duration = parseInt($(this).attr('data-duration'));
            var animation = duration / 4;

            setInterval(function () {
                counter++;
                if (counter >= quotes.length) {
                    counter = 0;
                }

                display.animate({
                    opacity: '0',
                }, animation, 'swing', function () {

                    display.text(quotes[counter]);

                    display.animate({
                        opacity: '1',
                    }, animation);
                });
            }, duration);
        });
    });


})(jQuery);